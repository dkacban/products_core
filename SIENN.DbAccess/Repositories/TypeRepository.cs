﻿using SIENN.DbAccess.Models;

namespace SIENN.DbAccess.Repositories
{
    public class TypeRepository : GenericRepository<Type>
    {
        public TypeRepository(ProductsContext context) : base(context)
        {
        }
    }
}
