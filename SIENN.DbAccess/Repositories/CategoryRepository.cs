﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Models;

namespace SIENN.DbAccess.Repositories
{
    public class CategoryRepository : GenericRepository<Category>
    {
        public CategoryRepository(ProductsContext context) : base(context)
        {
        }
    }
}
