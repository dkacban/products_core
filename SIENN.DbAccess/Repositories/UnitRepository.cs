﻿using SIENN.DbAccess.Models;

namespace SIENN.DbAccess.Repositories
{
    public class UnitRepository : GenericRepository<Unit>
    {
        public UnitRepository(ProductsContext context) : base(context)
        {
        }
    }
}
