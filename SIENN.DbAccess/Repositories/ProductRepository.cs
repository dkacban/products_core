﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Models;

namespace SIENN.DbAccess.Repositories
{
    public class ProductRepository : GenericRepository<Product>
    {
        public ProductRepository(ProductsContext context) : base(context)
        {
        }

        public override void Add(Product entity)
        {
            _entities.Add(entity);
            _context.SaveChanges();
        }
    }
}
