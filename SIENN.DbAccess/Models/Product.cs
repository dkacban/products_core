﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIENN.DbAccess.Models
{
    public class Product : BaseModel
    {
        public decimal Price { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime DeliveryDate { get; set; }

        public int TypeCode { get; set; }
        public Type Type { get; set; }

        public IEnumerable<Category> Categories { get; set; }

        public int UnitCode { get; set; }
        public Unit Unit { get; set; }
    }
}
