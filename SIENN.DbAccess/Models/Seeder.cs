﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SIENN.DbAccess.Models
{
    public static class Seeder
    {
        public static void Initialize(ProductsContext context)
        {
            context.Database.EnsureCreated();

            if (!context.Products.Any())
            {
                var types = new List<Type>()
                {
                    new Type {Description="Type 1"},
                    new Type {Description="Type 2"}
                };
                foreach (var type in types)
                {
                    context.Types.Add(type);
                }

                var categories = new List<Category>()
                {
                    new Category {Description="Category 1"},
                    new Category {Description="Category 2"}
                };
                foreach (var category in categories)
                {
                    context.Categories.Add(category);
                }

                var units = new List<Unit>()
                {
                    new Unit {Description="Pcs"},
                    new Unit {Description="Oz"}
                };
                foreach (var unit in units)
                {
                    context.Units.Add(unit);
                }

                var products = new List<Product>
                {
                    new Product {Categories=categories, DeliveryDate = DateTime.Now, IsAvailable = true, Price = 9.99m, Description="Orange Juice", Unit=units[0], Type=types[0] },
                    new Product {Categories=categories, DeliveryDate = DateTime.Now.AddDays(10), IsAvailable = true, Price = 2.99m, Description="Apples", Unit=units[1], Type=types[1] },
                    new Product {Categories=categories, DeliveryDate = DateTime.Now.AddMonths(1), IsAvailable = true, Price = 6.99m, Description="Bread", Unit=units[0], Type=types[0] },
                    new Product {Categories=categories, DeliveryDate = DateTime.Now.AddMonths(1), IsAvailable = false, Price = 6.99m, Description="Old product", Unit=units[0], Type=types[1] }
                };

                foreach (var product in products)
                {
                    context.Products.Add(product);
                }

                context.SaveChanges();
            }
        }
    }
}
