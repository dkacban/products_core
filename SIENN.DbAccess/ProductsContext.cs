﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Models;

namespace SIENN.DbAccess
{
    public class ProductsContext : DbContext
    {
        public ProductsContext(DbContextOptions<ProductsContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Type> Types { get; set; }
        public DbSet<Unit> Units { get; set; }
    }
}
