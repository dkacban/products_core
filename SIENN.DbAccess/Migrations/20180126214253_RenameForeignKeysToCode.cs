﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SIENN.DbAccess.Migrations
{
    public partial class RenameForeignKeysToCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Categories_CategoryId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Types_TypeId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Units_UnitId",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "UnitId",
                table: "Products",
                newName: "UnitCode");

            migrationBuilder.RenameColumn(
                name: "TypeId",
                table: "Products",
                newName: "TypeCode");

            migrationBuilder.RenameColumn(
                name: "CategoryId",
                table: "Products",
                newName: "CategoryCode");

            migrationBuilder.RenameIndex(
                name: "IX_Products_UnitId",
                table: "Products",
                newName: "IX_Products_UnitCode");

            migrationBuilder.RenameIndex(
                name: "IX_Products_TypeId",
                table: "Products",
                newName: "IX_Products_TypeCode");

            migrationBuilder.RenameIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                newName: "IX_Products_CategoryCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Categories_CategoryCode",
                table: "Products",
                column: "CategoryCode",
                principalTable: "Categories",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Types_TypeCode",
                table: "Products",
                column: "TypeCode",
                principalTable: "Types",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Units_UnitCode",
                table: "Products",
                column: "UnitCode",
                principalTable: "Units",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Categories_CategoryCode",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Types_TypeCode",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Units_UnitCode",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "UnitCode",
                table: "Products",
                newName: "UnitId");

            migrationBuilder.RenameColumn(
                name: "TypeCode",
                table: "Products",
                newName: "TypeId");

            migrationBuilder.RenameColumn(
                name: "CategoryCode",
                table: "Products",
                newName: "CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Products_UnitCode",
                table: "Products",
                newName: "IX_Products_UnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Products_TypeCode",
                table: "Products",
                newName: "IX_Products_TypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Products_CategoryCode",
                table: "Products",
                newName: "IX_Products_CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Categories_CategoryId",
                table: "Products",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Types_TypeId",
                table: "Products",
                column: "TypeId",
                principalTable: "Types",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Units_UnitId",
                table: "Products",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
