﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SIENN.DbAccess.Migrations
{
    public partial class MultipleCategoriesInProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Categories_CategoryCode",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CategoryCode",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CategoryCode",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "ProductCode",
                table: "Categories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Categories_ProductCode",
                table: "Categories",
                column: "ProductCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_Products_ProductCode",
                table: "Categories",
                column: "ProductCode",
                principalTable: "Products",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categories_Products_ProductCode",
                table: "Categories");

            migrationBuilder.DropIndex(
                name: "IX_Categories_ProductCode",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "ProductCode",
                table: "Categories");

            migrationBuilder.AddColumn<int>(
                name: "CategoryCode",
                table: "Products",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryCode",
                table: "Products",
                column: "CategoryCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Categories_CategoryCode",
                table: "Products",
                column: "CategoryCode",
                principalTable: "Categories",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
