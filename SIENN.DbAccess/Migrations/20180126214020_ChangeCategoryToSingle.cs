﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SIENN.DbAccess.Migrations
{
    public partial class ChangeCategoryToSingle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categories_Products_ProductCode",
                table: "Categories");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_Products_ProductCode",
                table: "Units");

            migrationBuilder.DropIndex(
                name: "IX_Units_ProductCode",
                table: "Units");

            migrationBuilder.DropIndex(
                name: "IX_Categories_ProductCode",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "ProductCode",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "ProductCode",
                table: "Categories");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Products",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_UnitId",
                table: "Products",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Categories_CategoryId",
                table: "Products",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Units_UnitId",
                table: "Products",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Categories_CategoryId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Units_UnitId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CategoryId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_UnitId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "ProductCode",
                table: "Units",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProductCode",
                table: "Categories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Units_ProductCode",
                table: "Units",
                column: "ProductCode");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_ProductCode",
                table: "Categories",
                column: "ProductCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_Products_ProductCode",
                table: "Categories",
                column: "ProductCode",
                principalTable: "Products",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_Products_ProductCode",
                table: "Units",
                column: "ProductCode",
                principalTable: "Products",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
