﻿using System.Collections.Generic;
using SIENN.DbAccess.Models;
using SIENN.Services.ViewModels;

namespace SIENN.Services
{
    public interface IProductService
    {
        IEnumerable<ProductViewModel> GetAvailableProductsPaged(PagingModel pagingModel);

        Product Get(int id);
    }
}
