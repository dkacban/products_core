﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIENN.Services.ViewModels
{
    public class PagingModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
