﻿namespace SIENN.Services.ViewModels
{
    public class ProductViewModel
    {
        public string ProductDescription { get; set; }
        public string Price { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public string Type { get; set; }
        public string IsAvailable { get; set; }
    }
}
