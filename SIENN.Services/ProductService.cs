﻿using System.Collections.Generic;
using SIENN.DbAccess.Models;
using SIENN.DbAccess.Repositories;
using SIENN.Services.ViewModels;
using System.Linq;
using AutoMapper;

namespace SIENN.Services
{
    public class ProductService : IProductService
    {
        IGenericRepository<Product> _repository;

        public ProductService(IGenericRepository<Product> repository)
        {
            _repository = repository;
        }

        public IEnumerable<ProductViewModel> GetAvailableProductsPaged(PagingModel pagingModel)
        {
            var products = _repository.GetAll().Skip(pagingModel.PageNumber - 1 * pagingModel.PageSize).Take(pagingModel.PageSize);

            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Product, ProductViewModel>()
                .ForMember(dest => dest.Type, 
                    opt => opt.ResolveUsing<TypeValueResolver>()
                ));

            var mapper = config.CreateMapper();

            var result = new List<ProductViewModel>();

            foreach(var product in products)
            {
                var productViewModel = mapper.Map<Product, ProductViewModel>(product);
                result.Add(productViewModel);
            }

            return result;
        }

        public Product Get(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
