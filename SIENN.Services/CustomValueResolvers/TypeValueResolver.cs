﻿using SIENN.DbAccess.Models;
using AutoMapper;
using SIENN.Services.ViewModels;

namespace SIENN.Services
{
    public class TypeValueResolver : IValueResolver<Product, ProductViewModel, string>
    {
        public string Resolve(Product source, ProductViewModel destination, string destMember, ResolutionContext context)
        {
            return $"({source.Type.Code}) {source.Type.Description}";
        }
    }
}
