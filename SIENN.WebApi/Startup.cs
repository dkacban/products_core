﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using SIENN.DbAccess;
using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Repositories;
using SIENN.DbAccess.Models;
using SIENN.Services;

namespace SIENN.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "SIENN Recruitment API"
                });
            });

            services.AddDbContext<ProductsContext>(o => o.UseSqlServer(Configuration.GetConnectionString("ProductsDatabase")));
            services.AddTransient<IGenericRepository<Type>, TypeRepository>();
            services.AddTransient<IGenericRepository<Unit>, UnitRepository>();
            services.AddTransient<IGenericRepository<Category>, CategoryRepository>();
            services.AddTransient<IGenericRepository<Product>, ProductRepository>();

            services.AddTransient<IProductService, ProductService>();

            services.AddMvc();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SIENN Recruitment API v1");
            });

            app.UseMvc();
        }
    }
}
