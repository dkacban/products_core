﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIENN.DbAccess.Repositories;
using SIENN.DbAccess.Models;
using AutoMapper;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        IGenericRepository<Product> _repository;

        public ProductController(IGenericRepository<Product> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IEnumerable<Product> Get()
        {
            return _repository.GetAll();
        }

        [HttpGet("{code}")]
        public Product Get(int code)
        {
            return _repository.Get(code);
        }

        [HttpPost]
        public void Post(Product product)
        {
            product.Code = 0;
            _repository.Add(product);
        }

        [HttpPut]
        public void Put(Product product)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Product, Product>();
            });
            var mapper = config.CreateMapper();

            var productToUpdate = _repository.Get(product.Code);
            mapper.Map<Product, Product>(product, productToUpdate);

            _repository.Save();
        }

        [HttpDelete("{code}")]
        public void Delete(int code)
        {
            var product = _repository.Get(code);
            _repository.Remove(product);
        }
    }
}