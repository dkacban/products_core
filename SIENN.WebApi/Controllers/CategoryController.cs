﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIENN.DbAccess.Repositories;
using SIENN.DbAccess.Models;
using AutoMapper;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        IGenericRepository<Category> _repository;

        public CategoryController(IGenericRepository<Category> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IEnumerable<Category> Get()
        {
            return _repository.GetAll();
        }

        [HttpGet("{code}")]
        public Category Get(int code)
        {
            return _repository.Get(code);
        }

        [HttpPost]
        public void Post(Category category)
        {
            category.Code = 0;
            _repository.Add(category);
        }

        [HttpPut]
        public void Put(Category category)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Category, Category>();
            });
            var mapper = config.CreateMapper();

            var categoryToUpdate = _repository.Get(category.Code);
            mapper.Map<Category, Category>(category, categoryToUpdate);

            _repository.Save();
        }

        [HttpDelete("{code}")]
        public void Delete(int code)
        {
            var category = _repository.Get(code);
            _repository.Remove(category);
        }
    }
}