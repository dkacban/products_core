﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SIENN.DbAccess.Repositories;
using SIENN.DbAccess.Models;
using AutoMapper;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Type")]
    public class TypeController : Controller
    {
        IGenericRepository<Type> _repository;

        public TypeController(IGenericRepository<Type> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IEnumerable<Type> Get()
        {
            return _repository.GetAll();
        }

        [HttpGet("{code}", Name = "Get")]
        public Type Get(int code)
        {
            return _repository.Get(code);
        }
        
        [HttpPost]
        public void Post(Type type)
        {
            type.Code = 0;
            _repository.Add(type);
        }
        
        [HttpPut]
        public void Put(Type type)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Type, Type>();
            });
            var mapper = config.CreateMapper();

            var typeToUpdate = _repository.Get(type.Code);
            mapper.Map<Type, Type>(type, typeToUpdate);

            _repository.Save();
        }
        
        [HttpDelete("{code}")]
        public void Delete(int code)
        {
            var type = _repository.Get(code);
            _repository.Remove(type);
        }
    }
}
