﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.Services.ViewModels;
using SIENN.DbAccess.Models;


namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ProductFilter")]
    public class ProductFilterController : Controller
    {
        IProductService _service;

        public ProductFilterController(IProductService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<ProductViewModel> GetAvailableProducts(PagingModel pagingModel)
        {
            return _service.GetAvailableProductsPaged(pagingModel);
        }
    }
}