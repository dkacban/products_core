﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIENN.DbAccess.Repositories;
using SIENN.DbAccess.Models;
using AutoMapper;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/unit")]
    public class UnitController : Controller
    {
        IGenericRepository<Unit> _repository;

        public UnitController(IGenericRepository<Unit> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IEnumerable<Unit> Get()
        {
            return _repository.GetAll();
        }

        [HttpGet("{code}")]
        public Unit Get(int code)
        {
            return _repository.Get(code);
        }

        [HttpPost]
        public void Post(Unit unit)
        {
            unit.Code = 0;
            _repository.Add(unit);
        }

        [HttpPut]
        public void Put(Unit unit)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Unit, Unit>();
            });
            var mapper = config.CreateMapper();

            var unitToUpdate = _repository.Get(unit.Code);
            mapper.Map<Unit, Unit>(unit, unitToUpdate);

            _repository.Save();
        }

        [HttpDelete("{code}")]
        public void Delete(int code)
        {
            var unit = _repository.Get(code);
            _repository.Remove(unit);
        }
    }
}