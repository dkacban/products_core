﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using SIENN.DbAccess.Models;
using SIENN.DbAccess;
using Microsoft.Extensions.DependencyInjection;

namespace SIENN.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                Seeder.Initialize(services.GetRequiredService<ProductsContext>());
                
            }

            BuildWebHost(args).Run();
        }


        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();



    }
}
